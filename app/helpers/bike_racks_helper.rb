module BikeRacksHelper
    #Return the full title on a per-page basic
    def full_title (page_title ='')
        base_title = "BikeRacks"
        if page_title.empty?
            base_title
        else
            page_title + " | " + base_title
        end
    end
end
