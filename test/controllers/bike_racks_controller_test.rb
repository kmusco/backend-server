require 'test_helper'

class BikeRacksControllerTest < ActionController::TestCase
  test "should get home" do
    get :home
    assert_response :success
    assert_select "title", "Home | BikeRacks"
  end

  test "should get help" do
    get :help
    assert_response :success
    assert_select "title", "Help | BikeRacks"
  end

  test "should get about" do
    get :about
    assert_response :success
    assert_select "title", "About | BikeRacks"
  end

  test "should get contact" do
    get :contact
    assert_response :success
    assert_select "title", "Contact | BikeRacks"
  end

end
